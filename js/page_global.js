$(document).ready(function() {
    // initPageWidth();
    initCommonItem();
    initLimitTxt();
    initGoTopBtn();
    // initHideBox();
});

function initHideBox (){
    $('.c_hideBox .c_showBtn').on('click',function(){
        var t=$(this);
        t.addClass('hide').siblings('.hideWrap').removeClass('hide');
    });
}


function initGoTopBtn() {


    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').removeClass('hide');
            //$('#header').addClass('fixedHeader');
        } else {
            $('.scrollToTop').addClass('hide');
            //$('#header')s.removeClass('fixedHeader');
        }

        if ($(this).scrollTop() > 0) {
            //$('.scrollToTop').removeClass('hide');
            $('#header').addClass('fixedHeader');
            // $('#menu').addClass('fixedMenu');

        } else {
            //$('.scrollToTop').addClass('hide');
            $('#header').removeClass('fixedHeader');
            // $('#menu').removeClass('fixedMenu');
        }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });



};

function EQBroHeight($bro) {
    var maxH = 0;;
    console.log($bro.length);
    $bro.each(function() {
        var t = $(this);
        var h = parseInt(t.css('height'), 10);
        console.log(h)
        maxH = (h > maxH) ? h : maxH;

    });

    $bro.css('height', maxH);

};
//限制字數並加上看更多
//elem 上要有 limitTxt class 跟 data-limit='字數'
function initLimitTxt() {

    var showBtn;

    $('.limitTxt').each(function() {
        var t = $(this);
        var $limit = parseInt(t.attr('data-limit'), 10);
        var $showBtn = (t.attr('data-showBtn') == 'true') ? true : false;


        if (typeof(t.attr('data-limitBtn')) === "undefined") {
            showBtn = '<a href="#" class="seeMoreContentBtn grayTxt f12">+展開全部</a>';
        } else {
            showBtn = t.attr('data-limitBtn');
        }

        if ($limit != undefined && $limit != '') {


            var $str = t.text(); // Getting the text
            $str = $str.replace(/ {2,}/g, ' ');
            $str = $str.replace(/\n\s*\n/g, '\n');

            if ($str.length > $limit) {

                var $strtemp = '<span class="hide">' + $str.substr($limit, $str.length) + '</span>' + showBtn;
                $str = $str.substr(0, $limit) + '<i class="dot"> ...</i>';


                if ($showBtn) {
                    $str += $strtemp;
                }

                t.html($str);

            }

        }


    });



    $('.seeMoreContentBtn').on('click', function(e) {
        e.preventDefault();
        $(this).addClass('hide').siblings('.hide').removeClass('hide').siblings('.dot').addClass('hide');
    });
}


// load 共同的項目
function initCommonItem() {
    $('#headerWrap').load('c_header.html');
    $('#footer').load('c_footer.html');
    //$('#popWrap').load('c_popWrap.html');
    // $('#memberBar').load('memberBar.html');
    // $('#parentOnlineBookStore').empty().load('parentOnlineBookStore.html');

}

//手機寬調整
function initPageWidth() {
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight || e.clientHeight || g.clientHeight;

    var viewport = document.querySelector("meta[name=viewport]");
    var result = x / 320;
    viewport.setAttribute('content', 'width=320, initial-scale=' + result + ',maximum-scale=' + result + ', user-scalable=1');
}



function initHeader() {

    var menuBtn = $('.h_menuBtn');
    var menu = $('#menu');
    var subNavBtn = menu.find('.mr_btn');
    var subNav = menu.find('.subNav');
    var clozBtn = $('#h_menuClozBtn');
    var bg = menu.find('.blackBg');

    menuBtn.on('click', toogleOpen);
    clozBtn.on('click', toogleOpen);
    subNavBtn.on('click', toogleSubOpen);
    bg.on('click', toogleOpen);

    function toogleOpen() {

        menu.toggleClass('hide');
        subNav.removeClass('show');
        $('html, body').animate({
            scrollTop: 0
        }, 0);
    };

    function toogleSubOpen() {
        var nav = $(this).siblings('.subNav');
        nav.toggleClass('show');
    };


}


// function initPop(){
//     initDropDown();
//     $('.popPage .clozBtn').on('click',function(){
//        $(this).closest('.popPage').css('display','none');
//     })
// }

// init 註冊登入的下拉選單
function initDropDown() {
    var yearBox = $('.year-select');
    var monthBox = $('.month-select');
    var year = new Date().getFullYear();

    yearBox.empty();
    monthBox.empty();


    yearBox.each(function() {
        var t = $(this);
        var val = t.attr('data-val');
        for (var i = 1900; i < year + 1; i++) {
            t.append('<option value="' + i + '">' + i + '</option>');
        }
        t.prepend('<option value="-1" selected>出生年</option>');
        if (val != undefined && val != '') {
            t.val(val);
        }



    });

    monthBox.each(function() {
        var t = $(this);
        var val = t.attr('data-val');
        for (var i = 1; i < 13; i++) {
            t.append('<option value="' + i + '">' + i + '</option>');
        }
        t.prepend('<option value="-1" selected>月</option>');
        if (val != undefined && val != '') {
            t.val(val);
        }

    });
}
